# VSCode Server

## Introduction
A quick way to spin up a VS Code server using Docker.

- Pre-Reqs:
    - docker
    - docker-compose

- Optional Pre-Reqs:
    - Projects folder
    - SSH keys
    - .gitconfig file

## Instructions
1. Copy `.env.example` to `.env`
2. Update variables in `.env`
3. Run `make start`
4. Access web UI at http://<ip_address>:8443
5. Enjoy

## Notes
- Volume Bind mounts (optional, but enabled)
    - `${HOME}/src` to `/config/workspace` -> aka projects home
    - `${HOME}/.ssh` to `/config/.ssh` -> mounts local SSH keys
    - `${HOME}/.gitconfig` to `/config/.gitconfig` -> mounts local .giconfig file 