# VSCode server Makefile

help:
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

start: ## start vscode server
	docker-compose up -d

stop: ## stop vscode server
	docker-compose down

update: ## update vscode server image
	docker-compose pull